"""
¡FUNCIONES!
Como idea general, las funciones las podemos ver como sub-tareas que nos ayudan
a realizar una tarea principal.
"""
def saluda():
  print("¡Hola mundo!")

"""
Las funciones pueden recibir PARÁMETROS (también llamados ARGUMENTOS) como
elementos de entrada para que nuestra función pueda realizar su tarea.

Un PARÁMETRO es un valor que la función espera recibir cuando sea llamada (invocada),
a fin de ejecutar acciones con base al mismo.
Una función puede esperar cero o varios parámetros (que irán separados por una coma **).
"""
def saludaA(nombre):
    print("¡Hola " + nombre + "!")

def suma(num1, num2):
    print("La suma de",num1,"y",num2,"es:",(num1+num2))

"""
En Python es posible asignar un valor por omisión a nuestros PARAMETROS.
"""
def hazPregunta(nombre=None, pregunta=""):
    if nombre == None:
        print("¡Hola!",pregunta)
    else:
        print("¡Hola ",nombre + "!", pregunta)

"""
Una función también puede recibir 'n' ARGUMENTOS.
"""
def sumaTodos(*numeros):
    suma = 0
    for numero in numeros:
        suma += numero # suma = suma + numero
    print("La suma de estos números es:",suma)

"""
Las funciones pueden RETORNAR un valor al finalizar el procesamiento
"""
def dameSuma(*numeros):
    suma = 0
    for numero in numeros:
        suma += numero # suma = suma + numero
    return suma

"""
La función MAIN (principal), es la función que es invocada al momento de ejecutar
el programa.
"""
if __name__ == "__main__":
    saluda() # OK
    # saludaA() # ERROR
    # saludaA("Roberto") # OK
    # suma(11,11) # OK
    # hazPregunta("Roberto") # OK
    # hazPregunta(nombre="Roberto", pregunta="¿Cómo te va?") # OK
    # hazPregunta("Roberto", "¿Cómo te va?") # OK
    # hazPregunta(pregunta="¿Cómo te va?") # OK
    # hazPregunta() # OK
    # sumaTodos() # OK
    # sumaTodos(1,2,3,4,5,6,7,8,9) # OK
    # suma = dameSuma(1,2,3,4,5,6,7,8,9) # OK
    # print("La suma es:", suma) # OK
    # suma = sumaTodos(1,2,3,4,5,6,7,8,9) # OK (No funciona como se esperaba)
