class Persona:
    def __init__(self, nombre, edad=None):
        self.nombre = nombre
        self.edad = edad
        self._secret = "Secreto"

    def saludo(self):
        print("Hola! mi nombre es",self.nombre)

class MiRedSocialFavorita:
    def __init__(self):
        self.usuarios = []

    def agregarUsuario(self, usuario):
        if usuario not in self.usuarios:
            self.usuarios.append(usuario)

    def conocerUsuarios(self):
        for usuario in self.usuarios:
            usuario.saludo()

    def posts(self):
        for usuario in self.usuarios:
            for post in usuario.posts:
                print(usuario.nombre, "publicó:",post)


class Usuario(Persona):
    def __init__(self, nombre, edad=None):
        super().__init__(nombre, edad)
        self.posts = []
        self.friends = []

    def post(self, text):
        p = Post(text)
        self.posts.append(p)
        return p

    def addFriend(self, friend):
        if friend not in self.friends:
            self.friends.append(friend)
            friend.addFriend(self)

    def friendsPosts(self):
        for friend in self.friends:
            for post in friend.posts:
                print(friend.name, "publicó:", post)

class Post(str):
    def __new__(cls, *args, **kwargs):
        return str.__new__(cls, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.likes = 0
        self.public = True

social = MiRedSocialFavorita()

u1 = Usuario("user 1")
social.agregarUsuario(u1)

u1.post("#UNAMDevChallenge")

u2 = Usuario("user 2")
social.agregarUsuario(u2)

social.conocerUsuarios()
social.posts()

u1.friendsPosts()

u1.addFriend(u2)
u1.friendsPosts()
