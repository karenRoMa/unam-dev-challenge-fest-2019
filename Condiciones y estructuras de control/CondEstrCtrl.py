"""" ************************************************************
             CONDICIONES Y ESTRUCTURAS DE CONTROL

        Una estructura de control dirije el orden de la
        ejecucion de las sentencias en un programa
**************************************************************"""

""""
 INTRODUCCION A LA SENTENCIA IF

if <expr>:
    <statement>

* <expr> es una expresion booleana
* <statement> debe ser una sentencia en Python (la cual debe ir identada)

Si <expr> es verdadera se ejecuta <statement>, si es falso, no se ejecuta <statement>

NOTA: la puntuacion (:) es requerida en Python a diferencia de los parentesis como en otros lenguajes

"""
x = 5
if x == 5:
    print("Estoy aprendiendo Python :D")

x = 0
y = 10

if x < y:
    print("0 es menor a 10")

# el valor numerico 0 equivale a falso
if x:
    print("x es verdadero")

if y:
    print("'y' existe y es diferente a 0 ")

if x or y:
    print("Alguno de x o y no es falso")

if x and y:
    print("'x' y 'y' son ambos falsos")

x = "0"

if x:
    print("'0' es una cadena que no equivale a false")

# Cadenas
if 'UNAM' in 'UNAM Mobile':
    print("Soy orgullosamente UNAM <3")

if 'UNAM' in ['MOBILE', 'DEV', 'CHALLENGE']:
    print("UNAM is everywhere")

"""" ************************************************************
                 AGRUPANDO SENTENCIAS
               -------------------------
                 IDENTACION Y BLOQUES
    Un bloque es considerado sintacticamente como una unidad.
    Dentro de una sentencia if si <expr> es verdadera se
    ejecuta todo un bloque

    ** Off-side rule: Los bloques se definen con identacion

    if <expr>
        <statement>
        <statement>
        ..
    <following_statement>

    * NOTA: En la documentacion de Python se refieren a los bloques
            con la expresion "suite"
**************************************************************"""

perritos = ['chihuahua', 'husky', 'pug', 'golden', 'akita']

if 'akita' in perritos:
    print("Los perritos son amor")
    print("Los perritos son bonitos")
    print("Los perritos son vida")
    print("... <3")
print("Akinota :O")

## Cada identacion implica un nuevo bloque

# Se ejecuta la sentencia?                   Si    No
#                                           ---    --
if 'foo' in ['foo', 'bar', 'baz']:        #  x
    print('Outer condition is true')      #  x

    if 10 > 20:                           #  x
        print('Inner condition 1')        #        x

    print('Between inner conditions')     #  x

    if 10 < 20:                           #  x
        print('Inner condition 2')        #  x

    print('End of outer condition')       #  x
print('After outer condition')            #  x

"""" ************************************************************
                 CLAUSULAS ELSE Y ELIF
               -------------------------
    if <expr>:
        <statement(s)>
    else:
        <statement(s)>

**************************************************************"""

## ELSE
x = 20
y = 50

if x < y:
    print("Primera 'suite'")
    print("x es muy chico")
else:
    print("Segunda 'suite'")
    print("y es muy chica")

## ELIF

name = "Karen"
if name == "Roberto":
    print("Hola Roberto")
elif name == "Aldo":
    print("Hola Aldo")
elif name == "Karen":
    print("Hola Karen :*")
elif name == "Mike":
    print("Hola Mike")
else:
    print("Quien eres?")

# Si no se agrega el else y ninguna de las condiciones es verdadera,
# simplemente no se ejecutara ningun bloque

"""" NOTA: en caso de que se encuentre un caso verdadero los siguientes no son ejecutados"""

if 'A' in 'UNAM':
    print('UNAM <3')
elif 1/0:
    print("This won't happen")
elif var:
    print("This won't either")


""""
SENTENCIA IF EN UNA LINEA
-------------------------
"""

if 5 < 30: print("5 es menor a 30")

# Las multiples sentencias se dividen con ;

if 30 < 100: print("30 es menor a 100"); print("30 pertenece a los naturales"); print("100 tambien pertenece a los naturales")

x = 2
if x == 1: print('x vale 1'); print('x no vale 2'); print('ok')
elif x == 2: print('x vale 2'); print('x no vale 1')
else: print('x no vale 1'); print('x no vale 2')

# No siempre es conveniente poner el if en una LINEA
debugging = True  # Set to True to turn debugging on.

if debugging: print('Debugging mode on')

"""" ************************************************************
               EXPRESIONES CONDICIONALES
               -------------------------
    Python soporta una entidad adicional llamada expresion
    condicional. A diferencia de las estructuras de control no
    definen el flujo de la ejecucion de un programa
    Actua mas como un operador para toma de decisiones

**************************************************************"""

raining = True
place = " a la playa" if not raining else " a ver Netflix"
print("Vamos"+place)

age = 12

status = 'menor' if age < 18 else 'adulto'

if status == 'adulto': print("Puede beber")

#
#
#
#
#
#
#
#
#
#

## EJERCICIO: ENCONTRAR EL MINIMO Y EL MAXIMO ENTRE DOS NUMEROS

"""" SOLUCION """
# Minimo
min = 0
x = 8
y = 299

if x < y:
    min = x
else:
    min = y
print("El minimo es "+str(min))

# Maximo
max = 0
x = 999
y = 888

if x > y:
    max = x
else:
    max = y
print("El maximo es "+str(max))

"""" ************************************************************
             SENTENCIA PASS
       En algunas ocasiones se requiere resguardar una
       sentencia if para cuando tengas lista la implementacion
       para eso existe la sentencia pass
**************************************************************"""

if 'e' in 'dev':
    pass


"""" ************************************************************
            SENTENCIA FOR
            -------------
        La sentencia for se utiliza para iterar sobre
        objetos 'iterables' (listas, strings, diccionarios, ...)
**************************************************************"""

for i in 'UNAM':
    print(i)

perritos = ['chihuahua', 'husky', 'pug', 'golden', 'akita']

for perrito in perritos:
    print("Los perritos "+perrito+" son amor")

numbers = [1, 5, 12, 91, 102]

for number in numbers:
    square = number*number
    print(str(number)+" al cuadrado es "+str(square))

my_list = range(0,10)
for i in my_list:
    print(i)
# range(x,y,z)
# x: primer elemento
# y: segundo elemento
# z: step

my_list = [1, 5, 12, 91, 102]
my_list_length = len(my_list)
for i in range(0,my_list_length):
    print(i, my_list[i] * my_list[i])
#
#
#
#
#
#
#
#
#
#
#
#

# EJERCICIO: imprimir python en forma de piramide

"""" SOLUCION """

my_string = "python"
x = 0

for i in my_string:
    x = x + 1
    print(my_string[0:x])

for i in my_string:
    x = x - 1
    print(my_string[0:x])

"""" ************************************************************
            SENTENCIA WHILE
            -------------
        No se especifica el numero de veces que se ejecuta
        el ciclo, en vez de eso se declara una condicion

        while <expr>:
            <statement(s)>
**************************************************************"""

n = 5
while n > 0:
    n -= 1
    print(n)

a = ['unam', 'dev', 'challenge']
while a:
    print(a.pop(-1))

# pop remueve elementos de la lista

""""  Interrupcion de una iteracion
* break: termina completamente el ciclo
* continue: se salta a la siguiente iteracion
  ----------------------------- """

n = 10
while n > 0 :
    n -= 1
    if n == 8:
        continue
    if n == 3:
        break
    print(n)
print('Se acabo el ciclo')

"""" SENTENCIA ELSE
while <expr>:
    <statement(s)>
else:
    <additional_statement(s)>
"""
## UNICO DE Python

n = 10
while n > 0:
    n -= 1
    print(n)
    if n == 2:
        break
else:
    print('Loop entro al else')


devChallenge = ['unam', 'mobile', 'dev', 'challenge']
word = 'hate'

index = 0
while index < len(devChallenge):
    if devChallenge[index] == word:
        print("There is "+word+" in dev challenge")
        break
    index += 1
else:
    print("There is no "+word+" in dev challenge")

## NOTA: Mejor forma de buscar elemento en una lista
if word in devChallenge:
    print(word, 'found in list.')
else:
    print(word, 'not found in list.')
""""
 WHILES ANIDADOS
while <expr1>:
    statement
    statement

    while <expr2>:
        statement
        statement
        break  # Rompe el ciclo while <expr2>:

    break  # Rompe el ciclo while <expr1>
 """

a = ['foo', 'bar']
while len(a):
    print(a.pop(0))
    b = ['baz', 'qux']
    while len(b):
        print('>', b.pop(0))

connection = True
time = 10
while connection:
    print("Estas conectado")
    while time > 0:
        time -=1
        print("Te quedan "+str(time)+" segundos")
        if time == 2:
            connection = False
else:
    print("Estas desconectado")

# WHILE en una linea

n = 10
while n > 0: n -= 1; print(n)

#
#
#
#
#
#
#
#
## EJERCICIO : HACER ULTIMO WHILE CON for iniciando desconectado

connection = False
time = range(0,9)
if not connection:
    print("Estas desconectado")
for remaining in time:
    print("Tiempo restante "+ str(10-remaining))
    if remaining == 2:
        connection = True
if connection:
    print("Estas conectado")
