"""" ************************************************************
             CONDICIONES Y ESTRUCTURAS DE CONTROL

        Una estructura de control dirije el orden de la
        ejecucion de las sentencias en un programa
**************************************************************"""

""""
 INTRODUCCION A LA SENTENCIA IF

if <expr>:
    <statement>

* <expr> es una expresion booleana
* <statement> debe ser una sentencia en Python (la cual debe ir identada)

Si <expr> es verdadera se ejecuta <statement>, si es falso, no se ejecuta <statement>

NOTA: la puntuacion (:) es requerida en Python a diferencia de los parentesis como en otros lenguajes

"""




"""" ************************************************************
                 AGRUPANDO SENTENCIAS
               -------------------------
                 IDENTACION Y BLOQUES
    Un bloque es considerado sintacticamente como una unidad.
    Dentro de una sentencia if si <expr> es verdadera se
    ejecuta todo un bloque

    ** Off-side rule: Los bloques se definen con identacion

    if <expr>
        <statement>
        <statement>
        ..
    <following_statement>

    * NOTA: En la documentacion de Python se refieren a los bloques
            con la expresion "suite"
**************************************************************"""





"""" ************************************************************
                 CLAUSULAS ELSE Y ELIF
               -------------------------
    if <expr>:
        <statement(s)>
    else:
        <statement(s)>

**************************************************************"""



"""" NOTA: en caso de que se encuentre un caso verdadero los siguientes no son ejecutados"""


""""
SENTENCIA IF EN UNA LINEA
-------------------------
"""




"""" ************************************************************
               EXPRESIONES CONDICIONALES
               -------------------------
    Python soporta una entidad adicional llamada expresion
    condicional. A diferencia de las estructuras de control no
    definen el flujo de la ejecucion de un programa
    Actua mas como un operador para toma de decisiones

**************************************************************"""


#
#
#
#
#
#
#
#
#
#

## EJERCICIO: ENCONTRAR EL MINIMO Y EL MAXIMO ENTRE DOS NUMEROS

"""" ************************************************************
             SENTENCIA PASS
       En algunas ocasiones se requiere resguardar una
       sentencia if para cuando tengas lista la implementacion
       para eso existe la sentencia pass
**************************************************************"""

if 'e' in 'dev':
    pass


"""" ************************************************************
            SENTENCIA FOR
            -------------
        La sentencia for se utiliza para iterar sobre
        objetos 'iterables' (listas, strings, diccionarios, ...)
**************************************************************"""

#
#
#
#
#
#
#
#
#
#
#

# EJERCICIO: imprimir python en forma de piramide

"""" SOLUCION """

"""" ************************************************************
            SENTENCIA WHILE
            -------------
        No se especifica el numero de veces que se ejecuta
        el ciclo, en vez de eso se declara una condicion

        while <expr>:
            <statement(s)>
**************************************************************"""


""""  Interrupcion de una iteracion
* break: termina completamente el ciclo
* continue: se salta a la siguiente iteracion
  ----------------------------- """



"""" SENTENCIA ELSE
while <expr>:
    <statement(s)>
else:
    <additional_statement(s)>
"""
## UNICO DE Python



""""
 WHILES ANIDADOS
while <expr1>:
    statement
    statement

    while <expr2>:
        statement
        statement
        break  # Rompe el ciclo while <expr2>:

    break  # Rompe el ciclo while <expr1>
 """


# WHILE en una linea

#n = 10
#while n > 0: n -= 1; print(n)
#
#
#
#
#
#
#
#
## EJERCICIO : HACER ULTIMO WHILE CON for iniciando desconectado
